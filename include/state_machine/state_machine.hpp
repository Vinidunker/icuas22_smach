#include <iostream>
#include <string>
#include <stdio.h>
#include <unistd.h>
#include <termios.h>
#include <vector>
#include <unistd.h>
#include <ros/ros.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2/convert.h>
#include <std_msgs/Bool.h>
#include <std_msgs/String.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/State.h>
#include <mavros_msgs/Altitude.h>
#include <mavros_msgs/PositionTarget.h>
#include <trajectory_msgs/MultiDOFJointTrajectoryPoint.h>
#include <trajectory_msgs/MultiDOFJointTrajectory.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <state_machine/CmdMsg.h>

enum DRONE_STATES{idle=0, translation=1, scanning=2, delivering=3};
enum DRONE_MODES{position=0, trajectory=1};
enum MISSION_STATUS{in_progress=0, done=1, failed=2};


class StateMachine
{
    private:

        ros::NodeHandle nh_;

    public:

        StateMachine(ros::NodeHandle & nh);

        ros::Publisher state_machine_pub;

        ros::Subscriber droneMsg_sub;
        ros::Subscriber challenge_sub;

        void stateMachine();

        void droneMsgCallback(const state_machine::CmdMsg::ConstPtr& msg);
        void challengeCallback(const std_msgs::Bool::ConstPtr& msg);

        int mode_, drone_state_;
};