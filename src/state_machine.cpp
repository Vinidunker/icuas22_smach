#include <state_machine/state_machine.hpp>

StateMachine::StateMachine(ros::NodeHandle & nh):
 nh_(nh)
{
    droneMsg_sub = nh_.subscribe<state_machine::CmdMsg>
            ("/drone/cmd", 10, &StateMachine::droneMsgCallback, this);
    
    challenge_sub = nh_.subscribe<std_msgs::Bool>
            ("/red/challenge_started", 10, &StateMachine::challengeCallback, this);

    state_machine_pub = nh_.advertise<state_machine::CmdMsg>
            ("/drone/state_machine", 10);
}

void StateMachine::challengeCallback(const std_msgs::Bool::ConstPtr& msg)
{
    if(msg->data)
    {
        ROS_INFO("Challenge Ready!");
        mode_ = DRONE_STATES::translation;
        stateMachine();
    }
}

void StateMachine::droneMsgCallback(const state_machine::CmdMsg::ConstPtr& msg)
{
    switch (msg->drone_state.data)
    {
        case DRONE_STATES::delivering:
            if (msg->mission_status.data == MISSION_STATUS::done)
            {
                mode_ = DRONE_STATES::idle;
                stateMachine();
            }
            break;

        case DRONE_STATES::translation:
            if (msg->mission_status.data == MISSION_STATUS::done)
            {
                mode_ = DRONE_STATES::scanning;
                stateMachine();
            }
            
            break;

        case DRONE_STATES::scanning:
            if (msg->mission_status.data == MISSION_STATUS::done)
            {
                mode_ = DRONE_STATES::delivering;
                stateMachine();
            }
            break;
    }
}

void StateMachine::stateMachine()
{
    state_machine::CmdMsg drone_msg;
    switch (mode_)
    {
        case DRONE_STATES::translation:
            ROS_INFO("State Machine: Translating throught the obstacles!");
            drone_msg.drone_state.data = DRONE_STATES::translation;
            state_machine_pub.publish(drone_msg);
            break;

        case DRONE_STATES::scanning:
            ROS_INFO("State Machine: Scanning for the 'Fire'!");
            drone_msg.drone_state.data = DRONE_STATES::scanning;
            state_machine_pub.publish(drone_msg);
            break;

        case DRONE_STATES::delivering:
            ROS_INFO("State Machine: Delivering the ball!");
            drone_msg.drone_state.data = DRONE_STATES::delivering;
            state_machine_pub.publish(drone_msg);
            break;

        case DRONE_STATES::idle:
            ROS_INFO("State Machine: Idle");
            drone_msg.drone_state.data = DRONE_STATES::idle;
            state_machine_pub.publish(drone_msg);
            break;
    }   
}

int main(int argc, char **argv)
{

    // initialize ROS and the node
    ros::init(argc, argv, "state_machine");
    ros::NodeHandle nh;

    StateMachine state_machine(nh);

    ros::Rate rate(50.0);
    
    ros::spinOnce();

    // wait for FCU connection
    while(ros::ok())
    {
        ros::spinOnce();
        rate.sleep();
    }

    return 0;
}